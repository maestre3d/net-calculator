﻿using System;
namespace Calculator
{
    public class Maestro
    {
		public void Init(){
			int o;
			double a = 0, b = 0, r = 0;


			Console.Clear();
			Console.WriteLine("Elije el tipo de operacion: \n1. Sumar\n2. Restar\n3. Dividir\n4. Multiplicar");
			o = int.Parse(Console.ReadLine());
			switch(o){
				case 1:
					r = Sumar(a, b);
					Console.Clear();
					Console.WriteLine("El resultado es {0}", r);
					Console.WriteLine("Desea realizar otra operacion?\n1. Si\n2. No");
					o = int.Parse(Console.ReadLine());
					if(o == 1){
						Init();
					}
					break;
				case 2:
					r = Restar(a, b);
                    Console.Clear();
                    Console.WriteLine("El resultado es {0}", r);
					Console.WriteLine("Desea realizar otra operacion?\n1. Si\n2. No");
                    o = int.Parse(Console.ReadLine());
                    if (o == 1)
                    {
                        Init();
                    }
					break;
				case 3:
					r = Dividir(a, b);
                    Console.Clear();
                    Console.WriteLine("El resultado es {0}", r);
					Console.WriteLine("Desea realizar otra operacion?\n1. Si\n2. No");
                    o = int.Parse(Console.ReadLine());
                    if (o == 1)
                    {
                        Init();
                    }
					break;
				case 4:
					r = Multiplicar(a, b);
                    Console.Clear();
                    Console.WriteLine("El resultado es {0}", r);
					Console.WriteLine("Desea realizar otra operacion?\n1. Si\n2. No");
                    o = int.Parse(Console.ReadLine());
                    if (o == 1)
                    {
                        Init();
                    }
                    break;
				default:
					Console.Clear();
					Console.WriteLine("Ninguna operacion seleccionada.");
					Console.ReadKey();
					Init();
					break;
			}
		}

		private double Sumar(double x, double y){
			Console.WriteLine("Valor de X:");
			x = double.Parse(Console.ReadLine());
			Console.WriteLine("Valor de Y:");
            y = double.Parse(Console.ReadLine());

			return x + y;
		}

		private double Restar(double x, double y)
        {
            Console.WriteLine("Valor de X:");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("Valor de Y:");
            y = double.Parse(Console.ReadLine());

            return x - y;
        }

		private double Dividir(double x, double y)
        {
            Console.WriteLine("Valor de X:");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("Valor de Y:");
            y = double.Parse(Console.ReadLine());

            return x / y;
        }

		private double Multiplicar(double x, double y)
        {
            Console.WriteLine("Valor de X:");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("Valor de Y:");
            y = double.Parse(Console.ReadLine());

            return x * y;
        }
    }
}
